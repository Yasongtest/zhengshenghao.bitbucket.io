function moon_texture(level, column, row) {
  let max_level = 5
  if (level <= max_level) {
    var new_row = Math.pow(2, level) - row - 1
    let x = null
    let y = null

    if (level <= 5) {
      x = zeroPad(column, 2)
      y = zeroPad(new_row, 2)
    }
    else {
      x = zeroPad(column, 4)
      y = zeroPad(new_row, 4)
    }
    const z = zeroPad(level, 2)
    var url = '../public/image/moon_6/' + z + '/' + x + '_' + y + '.png'
  }
  else {
    var url = undefined
    throw '123'
  }
  return url

  function zeroPad(num, places) {
    var zero = places - num.toString().length + 1;
    return Array(+(zero > 0 && zero)).join("0") + num;
  }
}

function ending(end) {
//  document.getElementById("overlay_true").style.display = "block" 
//  document.getElementById("overlay_false").style.display = "block" 
  if (end == true) {
document.getElementById("overlay_true").style.display = "block" 
  }
  if (end == false) {
  document.getElementById("overlay_false").style.display = "block"
  }
  if (end == undifined) {

  }

}
function reload(){
  location.reload(true)   
}
function mission_point() {
  missionlist.map(point => {
    let tag1 = new altizure.TagMarker({
      imgUrl: '../public/image/mission_point/' + point.id + '.jpg',
      // icon position
      position: point.position,
      // scene
      sandbox: sandbox,
      fixedSize: 55,
      //scale: 10000, 
      // pinLength:1000,
      depthTest:true
    })
    point.position.alt=1000000
    let tag2 = new altizure.TagMarker({
      imgUrl: '../public/image/mission_point/' + point.country + '.jpg',
      // icon position
      position: point.position,
      // scene
      sandbox: sandbox,
      fixedSize: 15,
      //scale: 10000, 
      // pinLength:1000,
      depthTest:true
    })
      tag1.pinLength = 1000000
      //tag2.position.alt=1000000
      tag2.pivot = {x: -1}

    
  })

}

var missionlist = [
  {
    id: 1,
    name: '阿波罗11号',
    position: {
      //0°40'26.69"N-23°28'22.69"E
      lng: 23.472969444444445,
      lat: 0.6740805555555556,
      alt: 1,
    },
    country: 'USA',
    type: 'man',
    description: '1969年7月16日，人类第一次登上月球。'

  },
  {
    id: 2,
    name: '阿波罗12号',
    position: {
      //3°0'44.60"S - 23°25'17.65"W
      lng: 23.421569444444444,
      lat: 3.012388888888889,
      alt: 1,
    },
    country: 'USA',
    type: 'man',
    description: '1969年11月14日，中国大陆第一次播出人类探索太空的实况录像。'
  },
  // {
  //   id: 3,
  //   name: '阿波罗14号',
  //   position: {
  //     lng:,
  //     lat:,
  //     alt: 1,
  //   },
  //   country: 'USA',
  //   type: 'man',
  // },
  {
    id: 4,
    name: '阿波罗15号',
    position: {
      //26° 7' 55.99" N-3° 38' 1.90"E
      lng: 3.633861111111111,
      lat: 26.132219444444445,
      alt: 1,
    },
    country: 'USA',
    type: 'man',
    description: '1971年7月26日，第一辆载人月球车。'
  },
  // {
  //   id: 5,
  //   name: '阿波罗16号',
  //   position: {
  //     //8°58'22.84"S-15°30'0.68"E
  //     lng: 15.50018888888889,
  //     lat: 8.973011111111111,
  //     alt: 1,
  //   },
  //   country: 'USA',
  //   type: 'man',
  // },
  // {
  //   id: 6,
  //   name: '阿波罗17号',
  //   position: {
  //     lng: 30.77168,
  //     lat: 20.19080,
  //     alt: 1,
  //   },
  //   country: 'USA',
  //   type: 'man',
  // },
  {
    id: 7,
    name: '嫦娥3号',
    position: {
      //44.1260°N 19.5014°W
      lng: -19.5014,
      lat: 44.1260,
      alt: 1,
    },
    country: 'cn',
    type: 'rover',
    description: '	2013年12月2日，中国成为世界上第三个实现月球软着陆的国家。'
  },
  {
    id: 8,
    name: '月球9号',
    position: {
      //7.08°N 64.37°W
      lng: -64.37,
      lat: 7.08,
      alt: 1,
    },
    country: 'USSR',
    type: 'Lander',
    description: '1966年1月31日，苏联实现第一次月球软着陆。'
  },
  {
    id: 9,
    name: '月球17号',
    position: {
      //	38°17'N 35°W
      lng: -35,
      lat: 38.28333333333333,
      alt: 1,
    },
    country: 'USSR',
    type: 'rover',
    description: '1970年11月10日，苏联第一辆月球车登月。'

  },
  {
    id: 10,
    name: '探测者1号',
    position: {
      //2.45°S 43.22°W
      lng: -43.22,
      lat: -2.45,
      alt: 1,
    },
    country: 'USSR',
    type: 'rover',
    description: '1966年5月30日，美国实现第一次月球软着陆。'
  },
  {
    id: 11,
    name: '月球21号',
    position: {
      //	38°17'N 35°W
      lng: -35,
      lat: 38.28333333333333,
      alt: 1,
    },
    country: 'USSR',
    type: 'rover',
    description: '1973年1月8日，月球车2号成为人类运行最远的月球车，运行距离39km。'

  },
]
