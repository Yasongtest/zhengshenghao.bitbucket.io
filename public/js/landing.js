 function moon_texture(level, column, row) {
          //天地图影像
          //var url =  'http://t'+Math.round(Math.random()*7)+'.tianditu.com/DataServer?T=img_w&x='+column+'&y='+row+'&l='+level
          //天地图矢量
          //var url = 'http://t'+Math.round(Math.random()*7)+'.tianditu.com/DataServer?T=vec_w&x='+column+'&y='+row+'&l='+level
          //高德影影像
          //var url = ' http://webst01.is.autonavi.com/appmaptile?lang=zh_cn&size=1&scale=1&style=6&x=' + column + '&y=' + row + '&z=' + level
          let max_level=6
          if (level <= max_level) {
            var new_row = Math.pow(2, level) - row - 1
            let x = null
            let y = null
            
            if (level <= 5) {
              x = zeroPad(column, 2)
              y = zeroPad(new_row, 2)
            }
            else {
              x = zeroPad(column, 4)
              y = zeroPad(new_row, 4)
            }
            const z = zeroPad(level, 2)
            var url = '../public/image/moon_6/' + z + '/' + x + '_' + y + '.png'
          }
          else {
            var url = undefined
          }

          //var url = 'http://127.0.0.1:8080/geoserver/gwc/service/tms/1.0.0/Kui_yong%3Atiff@EPSG%3A4326@png/'+level+'/'+column+'/'+new_row+'.png'
          return url

          function zeroPad(num, places) {
            var zero = places - num.toString().length + 1;
            return Array(+(zero > 0 && zero)).join("0") + num;
          }
        }
